module.exports = Behavior({
  behaviors: [],
  properties: {
    myBehaviorProperty: {
      type: String,
      value: 'Hello Behavior!'
    }
  },
  data: {
    myBehaviorData: {}
  },
  attached: function () {
    console.log(this.data.myBehaviorProperty)
  },
  methods: {
    myBehaviorMethod: function () { }
  }
})