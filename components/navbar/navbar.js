// components/navbar/navbar.js
const myBehavior = require('../../behavior/index.js')
Component({
  behaviors: [myBehavior],
  options: {
    multipleSlots: true, // 在组件定义时的选项中启用多slot支持
  },
  externalClasses: ['color'],
  /**
   * 组件的属性列表
   */
  properties: {
    val: {
      type: String,
      value: 123,
      observer: (newValue, oldValue) => {
        console.log(newValue, oldValue)
      }
    },
    testMyVal: String
  },
  methods: {
    sendMessageToParent() {
      this.triggerEvent('myEvent', { val: this.data.val })
    },
    add() {
      this.setData({
        val: this.data.val + 1
      })
    }
  },
  /**
   * 组件的初始数据
   */
  data: {
  }
})
