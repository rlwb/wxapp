// components/dialog/dialog.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    dialogStatus: {
      type: Boolean,
      value: false
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    closeDialog() {
      this.setData({
        dialogStatus: false
      })
    },
    showDialog() {
      this.setData({
        dialogStatus: true
      })
    },
    cancelMove(){
      return false;
    },
    cancelEvent() {
      
    }
  }
})
