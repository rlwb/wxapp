// pages/login/login.js
const http = require('../../http/index.js').http
var timer;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    token: 'f27be2e8-623d-4530-bba2-77dbdb9f7303'
  },
  //获取inpt值给token
  getInpt(val) {
    let value = val.detail.value;
    this.setData({
      token: value
    })
  },
  //扫码获取token
  scanning() {
    wx.scanCode({
      scanType: ['qrCode'],
      success: (res) => {
        console.log(res)
        this.setData({
          token: res.result
        })
      },
      fail: (err) => {
        console.log(err)
        wx.showToast({
          title: '扫码失败',
          icon: 'none'
        })
      }
    })
  },
  //登录
  login() {
    if(!this.data.token){
      wx.showToast({
        title: '请输入token',
        icon: none
      })
      return;
    }
    http('/accesstoken', {
      accesstoken: this.data.token
    }, 'POST')
    .then(res => {
      console.log(res)
      wx.showToast({
        title: '欢迎回来，' + res.loginname,
        icon: 'none'
      })
      timer = setTimeout(()=>{
        wx.switchTab({
          url: '/pages/index/index',
        })
      }, 1500)
      let userInfo = JSON.stringify(res)
      wx.setStorageSync('token', this.data.token)
      wx.setStorageSync('userInfo', userInfo)
    })
  },
  onHide() {
    clearTimeout(timer)
  }
})