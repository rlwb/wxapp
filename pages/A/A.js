// pages/A/A.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabList: [
      {
        label: '全部'
      },{
        label: '完成'
      },{
        label: '未完成'
      }
    ],
    val1: '321',
    cunrrentIndex: 0,
    dialog: false
  },
  add() {
    // this.setData({
    //   dialog: true
    // })
    this.dialog.showDialog()
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      val: this.data.tabList[this.data.cunrrentIndex].label
    })
  },
  changVal(e) {
    let val = e.detail.value;
    this.setData({
      val: val
    })
  },
  changTab(e) {
    let index = e.currentTarget.dataset.index;
    this.setData({
      val: this.data.tabList[index].label,
      cunrrentIndex: index
    })
  },
  getIndex(e) {
    if(e.target.dataset.id) {
      this.setData({
        val: e.target.dataset.id
      })
    }
  },
  test(val) {
    console.log(val)
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.navBar = this.selectComponent('#navBar')
    this.dialog = this.selectComponent("#dialog")
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})