// pages/logout/logout.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let userInfo = JSON.parse(wx.getStorageSync('userInfo'))
    this.setData({
      userInfo: userInfo
    })
    wx.setNavigationBarTitle({
      title: userInfo.loginname
    })
  },
  // 退出登录
  logout() {
    wx.clearStorageSync()
    wx.redirectTo({
      url: '/pages/login/login'
    })
  },
  //图片预览
  previewImg(e) {
    let url = e.currentTarget.dataset.src;
    wx.previewImage({
      urls: [url],
    })
  }
})