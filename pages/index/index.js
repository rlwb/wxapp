//index.js
//获取应用实例
const app = getApp()
const http = require('../../http/index.js').http
const moment = require('../../lib/moment.min.js')
moment.locale('zh-cn')
Page({
  data: {
    tabList: [
      {
        name: '全部',
        val: ''
      },
      {
        name: '精华',
        val: 'good'
      },
      {
        name: '分享',
        val: 'share'
      },
      {
        name: '问答',
        val: 'ask'
      },
      {
        name: '招聘',
        val: 'job'
      },
      {
        name: '客户端测试',
        val: ''
      }
    ],
    activedIndex: 0,
    scrollX: 0,
    page: 1,
    lists: [],
    tab: '',
    menuBtnHeight: 0,
    menuBtnTop: 0,
    menuBtnWidth: 0,
    menuBtnRight: 0,
    rpx2px: 1
  },
  //tab切换
  changeTab(e) {
    let index = e.currentTarget.dataset.index;
    let val = e.currentTarget.dataset.val;
    this.setData({
      activedIndex: index,
      tab: val,
      page: 1,
      lists: []
    })
    this.getLists()
    if(index === 5) {
      this.setData({
        scrollX: 200
      })
    }else if(index === 0){
      this.setData({
        scrollX: 0
      })
    }
  },
  getLists() {
    let str = `lists[${this.data.page-1}]`
    http('/topics', {
      page:this.data.page,
      tab: this.data.tab,
      limit: 30
    })
    .then(res => {
      for(let i = 0; i < res.data.length; i ++) {
        res.data[i].last_reply_at = moment(res.data[i].last_reply_at).fromNow()
      }
      this.setData({
        [str]: res.data,
        page: this.data.page + 1
      })
    })
  },
  onLoad() {
    this.getLists();
    const data = wx.getMenuButtonBoundingClientRect()
    console.log(data)

    
    wx.getSystemInfo({
      success: (res)=> {
        let rpx2px = res.screenWidth/750;
        this.setData({
          menuBtnHeight: data.height,
          menuBtnTop: data.top,
          rpx2px: rpx2px,
          menuBtnRight: res.screenWidth - data.left - data.width,
          menuBtnWidth: data.width
        })
      },
    })
  },
  //跳转详情页面
  toDetails(e) {
    let id = e.currentTarget.id
    wx.navigateTo({
      url: '/pages/details/details?id=' + id,
    })
  }
})
