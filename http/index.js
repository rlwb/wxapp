// const baseUrl = 'https://cnodejs.org/api/v1'//开发
// const baseUrl = 'https://cnodejs.org/api/v1'//测试
const baseUrl = 'https://cnodejs.org/api/v1'//正式


function http(url, data, method) {
  wx.showLoading({
    title: '加载中...',
  })
  return new Promise((resolve, reject)=> {
    wx.request({
      url: baseUrl + url,
      data,

      method,
      success: (res) =>{
        if(res.statusCode === 200) {
          resolve(res.data)
        }else {
          if(res.statusCode === 401){
            wx.redirectTo({
              url: '/pages/login/login',
            })
          }
          reject(res)
        }
      },
      fail: (err) => {
        reject(err)
      },
      complete: (res) => {
        wx.hideLoading()
      }
    })
  })
}

module.exports = {
  http
}